﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonorApp.Models
{
    public class ChangePasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        public string password { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string newPassword { get; set; }
        [Required]
        [DataType(DataType.Password)]
        [Compare("newPassword", ErrorMessage ="Password does not match")]
        public string confirmPassword { get; set; }
    }
}
