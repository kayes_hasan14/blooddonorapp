﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonorApp.Models
{
    public class AuthorizedModel
    {
        [Required]
        public string RoleName { get; set; }
    }
}
