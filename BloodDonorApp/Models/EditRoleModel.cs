﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BloodDonorApp.Models
{
    public class EditRoleModel
    {
        public String Id { get; set; }
        [Required (ErrorMessage = "Enter Role Name")]
        public string RoleName { get; set; }
        public List<string> User { get; set; }
    }
}
