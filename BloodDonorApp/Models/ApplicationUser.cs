﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonorApp.Models
{
    public class ApplicationUser : IdentityUser
    {
        [Display(Name = "Name")]
        [Required]
        public string Name { get; set; }
        [Display(Name = "Date Of Birth")]
        [Required]
        public DateTime DateOfBirth { get; set; }
        [Display(Name = "Blood Group")]
        [Required]
        public string BloodGroup { get; set; }
        [Display(Name="Last date of Donation")]
        public DateTime LastDonated { get; set; }

    }
}
