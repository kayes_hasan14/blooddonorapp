﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonorApp.Models
{
    public class BloodDonorDbContext : IdentityDbContext<ApplicationUser>
    {
        public BloodDonorDbContext(DbContextOptions<BloodDonorDbContext> options) :base(options)
        {

        }
       
    }
}
