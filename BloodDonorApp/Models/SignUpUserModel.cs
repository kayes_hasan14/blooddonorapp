﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonorApp.Models
{
    public class SignUpUserModel
    {
        [Display(Name = "Name")]
        [Required]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Enter A Valid Email Address")]
        public string email { get; set; }

        [Required]
        [Display(Name = "Password")]
        [Compare("ConfirmPassword",ErrorMessage ="Password doesnot match")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(ErrorMessage = "Enter password again")]
        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
        [Display(Name = "Date Of Birth")]
        [Required]
        public DateTime DateOfBirth { get; set; }
        [Display(Name = "Blood Group")]
        [Required]
        public string BloodGroup { get; set; }
        [Display(Name = "Date of Last Donation")]
        public DateTime LastDonated { get; set; }

    }
}
