﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonorApp.Models
{
    public class EditUserDetails
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string BloodGroup { get; set; }
        [Display(Name = "Date of Last Donation")]
        public DateTime LastDonated { get; set; }
    }
}
