﻿using BloodDonorApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BloodDonorApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AuthorizationController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public AuthorizationController(RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }
        [HttpGet]
        public IActionResult CreateRole()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> CreateRole(AuthorizedModel model)
        {
            if (ModelState.IsValid)
            {
                IdentityRole newRole = new IdentityRole
                {
                    Name = model.RoleName
                };
                IdentityResult result = await _roleManager.CreateAsync(newRole);
                if (!result.Succeeded)
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError("", error.Description);
                    }
                }

                ModelState.Clear();
                return RedirectToAction("getRoleList", "Authorization");
            }
            return View();
        }

        [HttpGet]
        public IActionResult getRoleList()
        {
            List<IdentityRole> roles = _roleManager.Roles.OrderBy(x => x.Name).ToList();
            return View(roles);
        }
        [HttpGet]
        async public Task<IActionResult> Edit(string id)
        {
            var role = await _roleManager.FindByIdAsync(id);
            var model = new EditRoleModel
            {
                RoleName = role.Name,
                Id = role.Id
            };

           

            return View(model);
        }
        [HttpPost]
        async public Task<IActionResult> Edit(EditRoleModel model)
        {
            var role = await _roleManager.FindByIdAsync(model.Id);
            role.Name = model.RoleName;
            var result = await _roleManager.UpdateAsync(role);

            if (result.Succeeded)
            {
                return RedirectToAction("getRoleList");
            }
            else
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }

            return View(model);
        }

        [HttpGet]
        async public Task<IActionResult> EditUsersInRole(string id)
        {
            ViewBag.Id = id;
            var role = await _roleManager.FindByIdAsync(id);
            var model = new List<UserRoleModel>();
            foreach (var user in _userManager.Users)
            {
                var userRoleModel = new UserRoleModel()
                {
                    UserId = user.Id,
                    UserName = user.UserName
                };
                if (await _userManager.IsInRoleAsync(user, role.Name))
                {
                    userRoleModel.IsSelected = true;
                }
                else
                {
                    userRoleModel.IsSelected = false;
                }
                               
                model.Add(userRoleModel);
            }
            return View(model);
        }
        [HttpPost]
        async public Task<IActionResult> EditUsersInRole(List<UserRoleModel> model, string id)
        {
            var role = await _roleManager.FindByIdAsync(id);
            for (int i = 0; i < model.Count; i++)
            {
                var user = await _userManager.FindByIdAsync(model[i].UserId);
                IdentityResult result = null;
                if (model[i].IsSelected && !(await _userManager.IsInRoleAsync(user, role.Name)))
                {
                    result = await _userManager.AddToRoleAsync(user, role.Name);
                }
                else if (!model[i].IsSelected && await _userManager.IsInRoleAsync(user, role.Name))
                {
                    result = await _userManager.RemoveFromRoleAsync(user, role.Name);
                }
                else
                {
                    continue;
                }
                if (i < model.Count - 1)
                {
                    continue;
                }
                else
                {
                    return RedirectToAction("EditUsersInRole", new { id = id });
                }
            }
            return RedirectToAction("EditUsersInRole", new { id = id });
        }

        [HttpGet]
        public async Task<IActionResult>  Delete(string Id)
        {
            var role =await _roleManager.FindByIdAsync(Id);
            var result =await _roleManager.DeleteAsync(role);
            return View("getRoleList");
        }

    }
}
