﻿using BloodDonorApp.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace BloodDonorApp.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IHttpContextAccessor _httpContext;

        public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, 
            IHttpContextAccessor httpContext, RoleManager<IdentityRole> roleManager)
        {

            _userManager = userManager;
            _signInManager = signInManager;
            _httpContext = httpContext;
            _roleManager = roleManager;
        }

        [HttpGet]
        [Route("SignUp")]
        public IActionResult SignUp()
        {
            return View();
        }

        [Route("SignUp")]
        [HttpPost]

        public async Task<IActionResult> SignUp(SignUpUserModel signUpUserModel)
        {
            if(!(_userManager.Users.Any()))
            {
                if (ModelState.IsValid)
                {
                    var user = new ApplicationUser()
                    {
                        Name = signUpUserModel.Name,
                        DateOfBirth = signUpUserModel.DateOfBirth,
                        BloodGroup = signUpUserModel.BloodGroup,
                        Email = signUpUserModel.email,
                        UserName = signUpUserModel.email,
                        LastDonated = signUpUserModel.LastDonated
                    };
                    var newRegisteredUser = await _userManager.CreateAsync(user, signUpUserModel.Password);
                    if (!newRegisteredUser.Succeeded)
                    {
                        foreach (var errors in newRegisteredUser.Errors)
                        {
                            ModelState.AddModelError("", errors.Description);
                        }
                        return View(signUpUserModel);
                    }
                    ModelState.Clear();
                    
                    IdentityRole newRole = new IdentityRole
                    {
                        Name = "Admin"
                    };
                    IdentityResult role = await _roleManager.CreateAsync(newRole);
                    IdentityResult result = await _userManager.AddToRoleAsync(user, newRole.Name );
                   
                }
            }
            else
            {
                if (ModelState.IsValid)
                {
                    var user = new ApplicationUser()
                    {
                        Name = signUpUserModel.Name,
                        DateOfBirth = signUpUserModel.DateOfBirth,
                        BloodGroup = signUpUserModel.BloodGroup,
                        Email = signUpUserModel.email,
                        UserName = signUpUserModel.email,
                        LastDonated = signUpUserModel.LastDonated
                    };
                    var result = await _userManager.CreateAsync(user, signUpUserModel.Password);
                    if (!result.Succeeded)
                    {
                        foreach (var errors in result.Errors)
                        {
                            ModelState.AddModelError("", errors.Description);
                        }
                        return View(signUpUserModel);
                    }
                    ModelState.Clear();
                }
            }
            //var signedIn = await _signInManager.PasswordSignInAsync(signUpUserModel.email, signUpUserModel.Password, false, false);
            return RedirectToAction("SignIn");
        }

        [HttpGet]
        [Route("SignIn")]

        public IActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        [Route("SignIn")]

        public async Task<IActionResult> SignIn(SignInModel user, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var result =await _signInManager.PasswordSignInAsync(user.Email, user.Password, user.rememberMe, false);
                if(result.Succeeded)
                {
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        return LocalRedirect(returnUrl);
                    }
                        return RedirectToAction("Index","Home");
                }
                ModelState.AddModelError("","Login Failed");
                
            }
            return View();
        }

        public async Task<IActionResult> SignOut()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [Route("Change-Password")]
        public IActionResult ChangePassword()
        {
            return View();
        }
        [Route("Change-Password")]
        [HttpPost]
       async public Task<IActionResult> ChangePassword(ChangePasswordModel newpass)
        {

        
            var userData = await _userManager.GetUserAsync(User);
            var rs = await _userManager.ChangePasswordAsync(userData, newpass.password, newpass.newPassword);
            
            //var result = await _user.ChangeEmailAsync(userData, newpass.password, newpass.newPassword);
            if(rs.Succeeded)
            {
                ViewBag.IsSucceed = true;
                ModelState.Clear();
                await _signInManager.SignOutAsync();
                return RedirectToAction("SignIn","Account");
            }
            foreach(var error in rs.Errors)
            {
               
                ModelState.AddModelError("",error.Description);
            } 
            return View(newpass);
        }
        
        public IActionResult GetDonorList()
        {
            List<ApplicationUser> all = _userManager.Users.OrderByDescending(x => x.LastDonated).ToList();
            return View(all);
        }
       
    }
}
